import { Fragment, h, VNode } from 'preact';

// -----------------------------------------------------------------------------

export default function ResumeExpertise({ skill, duration }: ResumeExpertiseProps): VNode {
	return (
		<Fragment>
			<strong>{skill}</strong> <span>({duration})</span>
		</Fragment>
	);
}

// -----------------------------------------------------------------------------

interface ResumeExpertiseProps {
	skill: string;
	duration: string;
}
