import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';
import Header from 'component/Shared/SectionHeader';

import ResumeExpertise from './Expertise';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExpertises({ expertise = [] }: ResumeExpertiseProps): VNode {
	return expertise.length ? (
		<Content containerClass={c.container}>
			<Header label="Expertise" />
			<ul>{renderExpertises(expertise)}</ul>
		</Content>
	) : null;
}

// -----------------------------------------------------------------------------

function renderExpertises(expertise: ResumeExpertiseEntry[] = []): VNode[] {
	return expertise.map(renderExpertise);
}

function renderExpertise([skill, duration]: ResumeExpertiseEntry, key: number): VNode {
	return (
		<li key={key}>
			<ResumeExpertise skill={skill} duration={duration} />
		</li>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeExpertiseProps {
	expertise: ResumeExpertiseEntry[];
}

interface ResumeExpertiseEntry extends Array<string> {
	[0]: string;
	[1]: string;
}
