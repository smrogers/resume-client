import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeHeader({
	fullName,
	byline,
	contactEmail,
	contactPhoneNumber,
}: ResumeHeaderProps): VNode {
	return (
		<Content containerClass={c.container} contentClass={c.content}>
			<div class={c.details}>
				<div class={c.primary}>
					<h1 class={c.fullName}>{fullName}</h1>
					<h2 class={c.byline}>{byline}</h2>
				</div>

				<div class={c.secondary}>
					<span>
						<a href={`mailto:${contactEmail}?subject=Resume`}>{contactEmail}</a>
					</span>
					<span>{contactPhoneNumber}</span>
				</div>
			</div>
		</Content>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeHeaderProps {
	fullName: string;
	byline?: string;
	contactEmail?: string;
	contactPhoneNumber?: string;
}
