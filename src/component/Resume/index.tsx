import { h, VNode } from 'preact';

import Advertisement from './Advertisement';
import Header from './Header';
import Experiences from './Experiences';
import Expertises from './Expertises';
import ResumePersonalStatement from './PersonalStatement';
import References from './References';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function Resume({ response: resume }): VNode {
	return (
		<div class={c.container}>
			<Advertisement />
			<Header {...resume} />
			<ResumePersonalStatement {...resume} />
			<Expertises {...resume} />
			<References {...resume} />
			<Experiences {...resume} />
		</div>
	);
}
