import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';
import Header from 'component/Shared/SectionHeader';

import ResumeReference from './Reference';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeReferences({ reference = [] }: ResumeReferenceProps): VNode {
	return reference.length ? (
		<Content containerClass={c.container}>
			<Header label="References" />
			<ul>{renderReferences(reference)}</ul>
		</Content>
	) : null;
}

// -----------------------------------------------------------------------------

function renderReferences(references: ReferenceEntry[] = []): VNode[] {
	return references.map(renderReference);
}

function renderReference([name, contact]: ReferenceEntry, key: number): VNode {
	return (
		<li>
			<ResumeReference key={key} name={name} contact={contact} />
		</li>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeReferenceProps {
	reference: ReferenceEntry[];
}

interface ReferenceEntry extends Array<string> {
	[0]: string;
	[1]: string;
}
