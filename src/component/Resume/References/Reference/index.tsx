import { Fragment, h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeReference({ name, contact }: ResumeReferenceProps): VNode {
	return (
		<div class={c.reference}>
			<strong>{name}</strong>
			<span>{contact}</span>
		</div>
	);
}

// -----------------------------------------------------------------------------

interface ResumeReferenceProps {
	name: string;
	contact: string;
}
