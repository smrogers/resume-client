import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumePersonalStatement({
	personalStatement,
}: ResumePersonalStatementProps): VNode {
	return personalStatement ? (
		<Content containerClass={c.container} contentClass={c.content}>
			{personalStatement}
		</Content>
	) : null;
}

// typing
// -----------------------------------------------------------------------------

interface ResumePersonalStatementProps {
	personalStatement?: string;
}
