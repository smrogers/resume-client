import { Fragment, h, VNode } from 'preact';

import Content from 'component/Shared/Content';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeAdvertisement(): VNode {
	return (
		<Fragment>
			<Content containerClass={c.container} contentClass={c.content}>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					fill="none"
					viewBox="0 0 24 24"
					stroke="currentColor"
					width="12"
					height="12"
				>
					<path
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z"
					/>
				</svg>
				<a href="#" onClick={onPrintButtonResumeClicked}>
					print
				</a>
				<span></span>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					fill="none"
					viewBox="0 0 24 24"
					stroke="currentColor"
					width="12"
					height="12"
				>
					<path
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
					/>
				</svg>
				<a href={EDITOR_URL}>make your own</a>
				<span></span>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					fill="none"
					viewBox="0 0 24 24"
					stroke="currentColor"
					width="12"
					height="12"
				>
					<path
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4"
					/>
				</svg>
				<a href="https://bitbucket.org/smrogers/resume-client">source</a>
			</Content>
			<Content containerClass={c.fakeContainer} contentClass={c.content}>
				&nbsp;
			</Content>
		</Fragment>
	);
}

// -----------------------------------------------------------------------------

function onPrintButtonResumeClicked(e: Event): boolean {
	e.preventDefault();
	e.stopPropagation();

	//
	window.print();

	//
	return false;
}
