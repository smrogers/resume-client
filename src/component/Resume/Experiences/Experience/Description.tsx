import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

const REGEX_MULTI_NEWLINE = /\n+/g;

// -----------------------------------------------------------------------------

export default function ResumeExperienceDescription({
	description,
}: ResumeExperienceDescriptionProps): VNode {
	return description ? (
		<div class={c.description}>{renderDescriptionContent(description)}</div>
	) : null;
}

// -----------------------------------------------------------------------------

function renderDescriptionContent(description: string = ''): VNode[] {
	return description.split(REGEX_MULTI_NEWLINE).map(toParagraph);
}

function toParagraph(paragraph: string, key: number): VNode {
	return <p key={key} dangerouslySetInnerHTML={{ __html: applyHyperlinks(paragraph) }} />;
}

function applyHyperlinks(text = ''): string {
	return text.replace(/(https?:\/\/[^\s]+)/gi, '<a href="$1">$1</a>');
}

// -----------------------------------------------------------------------------

interface ResumeExperienceDescriptionProps {
	description: string;
}
