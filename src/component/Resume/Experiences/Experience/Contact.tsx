import { Fragment, h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperienceContact({
	contact: [name, numberOrEmail] = [],
}: ResumeExperienceContactProps): VNode {
	return name ? (
		<div class={c.contact}>
			<strong>Contact:</strong> {name} {renderContact(numberOrEmail)}
		</div>
	) : null;
}

// -----------------------------------------------------------------------------

function renderContact(contact): VNode {
	return contact ? <Fragment>&mdash; {contact}</Fragment> : null;
}

// -----------------------------------------------------------------------------

interface ResumeExperienceContactProps {
	contact: string[];
}
