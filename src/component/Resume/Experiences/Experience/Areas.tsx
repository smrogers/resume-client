import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperienceAreas({ areas }: ResumeExperienceAreasProps): VNode {
	return areas ? <div class={c.areas}>{areas}</div> : null;
}

// -----------------------------------------------------------------------------

interface ResumeExperienceAreasProps {
	areas: string;
}
