import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperienceHeader({
	title,
	timeframe,
}: ResumeExperienceDescriptionProps): VNode {
	return (
		<div class={c.header}>
			<strong>{title}</strong>
			<em>{timeframe}</em>
		</div>
	);
}

// -----------------------------------------------------------------------------

interface ResumeExperienceDescriptionProps {
	title: string;
	timeframe: string;
}
