import { h, VNode } from 'preact';

import Areas from './Areas';
import Contact from './Contact';
import Description from './Description';
import Header from './Header';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperience(experience: ResumeExperienceProps): VNode {
	return (
		<div class={c.experience}>
			<Header {...experience} />
			<Areas {...experience} />
			<Contact {...experience} />
			<Description {...experience} />
		</div>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeExperienceProps {
	areas: string;
	contact: string[];
	description: string;
	title: string;
	timeframe: string;
}
