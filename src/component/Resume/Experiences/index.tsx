import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';
import Header from 'component/Shared/SectionHeader';

import ResumeExperience from './Experience';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperiences({ experience = [] }: ResumeExperiencesProps): VNode {
	return experience.length ? (
		<Content containerClass={c.container}>
			<Header label="Experiences" />
			<ul>{renderExperiences(experience)}</ul>
		</Content>
	) : null;
}

// -----------------------------------------------------------------------------

function renderExperiences(experiences: ResumeExperienceEntry[] = []): VNode[] {
	return experiences.map(renderExperience);
}

function renderExperience(experience: ResumeExperienceEntry, key: number): VNode {
	return (
		<li key={key}>
			<ResumeExperience {...experience} />
		</li>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeExperiencesProps {
	experience: ResumeExperienceEntry[];
}

interface ResumeExperienceEntry {
	areas: string;
	contact: string[];
	description: string;
	title: string;
	timeframe: string;
}
