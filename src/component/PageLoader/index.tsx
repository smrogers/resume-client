import { h, VNode } from 'preact';
import { useEffect, useState } from 'preact/hooks';

import LoadingIndicator from 'component/Shared/LoadingIndicator';

import ErrorComponent from './Error';

// -----------------------------------------------------------------------------

export default function PageLoader({ endpoint, payload, Component }: PageLoaderProps): VNode {
	const [response, setResponse] = useState(undefined);

	//
	useEffect(ofContactingApiEndpoint(endpoint, payload, setResponse), [
		endpoint,
		payload,
		Component,
	]);

	return response === undefined ? <LoadingIndicator /> : renderResponse(Component, response);
}

// -----------------------------------------------------------------------------

function renderResponse(Component: PageComponent, response: GenericObject): VNode {
	const Renderer: PageComponent = response?.success ? Component : ErrorComponent;

	return <Renderer response={response} />;
}

// -----------------------------------------------------------------------------

function ofContactingApiEndpoint(
	endpoint: string,
	payload: any = undefined,
	setResponse: Function,
) {
	return () => {
		fetch(`/api/${endpoint}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(payload),
		})
			.then(checkForErrorStatus)
			.then(getJson)
			.then((data) => setResponse(data))
			.catch((error) =>
				setResponse({
					success: false,
					message: error.message,
				}),
			);
	};
}

function checkForErrorStatus(response) {
	if (response.status !== 200 && response.status !== 204) {
		throw new Error(`${response.status} ${response.statusText}`);
	}

	return response;
}

function getJson(response) {
	return response.json();
}

// -----------------------------------------------------------------------------

type PageLoaderProps = {
	endpoint: string;
	payload?: GenericObject;
	Component: PageComponent;
};

interface PageComponent extends Function {
	response?: GenericObject;
}
