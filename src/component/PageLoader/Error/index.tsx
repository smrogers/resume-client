import { h, VNode } from 'preact';

import Content from 'component/Shared/Content';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function PageLoaderError({ response }: PageLoaderErrorProps): VNode {
	return (
		<Content contentClass={c.content}>
			<h1>Something went wrong</h1>
			<p>{response?.message || 'There was an error'}</p>
		</Content>
	);
}

// -----------------------------------------------------------------------------

type PageLoaderErrorProps = {
	response?: GenericObject;
};
