import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function LoadingIndicator(): VNode {
	return (
		<div class={c.loadingIndicator}>
			<svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256">
				<circle cx="128" cy="128" r="122" />
			</svg>
		</div>
	);
}
