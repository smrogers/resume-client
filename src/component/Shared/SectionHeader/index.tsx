import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeSectionHeader({ label }): VNode {
	return <h3 class={c.sectionHeader}>{label}</h3>;
}
