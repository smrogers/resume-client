import { h, VNode } from 'preact';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function Content({ containerClass, contentClass, children }: ContentProps): VNode {
	return (
		<div class={className(c.container, containerClass)}>
			<div class={className(c.content, contentClass)}>{children}</div>
		</div>
	);
}

// -----------------------------------------------------------------------------

function className(...classNames: string[]): string {
	return classNames.filter(identity).join(' ');
}

// This was the only lodash function I used in this entire project.
// Now that's some old-school tree-shaking.
function identity(value: any): any {
	return value;
}

// -----------------------------------------------------------------------------

type ContentProps = {
	containerClass?: string;
	contentClass?: string;
	children?: any;
};
