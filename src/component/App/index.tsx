import './style.scss';

import { h, render, VNode } from 'preact';

import Resume from 'component/Resume';
import PageLoader from 'component/PageLoader';

// -----------------------------------------------------------------------------

export default function renderApp(): void {
	render(<App />, document.body);

	//
	const loaderStyle = document.getElementById('loaderStyle');
	loaderStyle.parentNode.removeChild(loaderStyle);
}

// -----------------------------------------------------------------------------

function App(): VNode {
	const { pathname = '/' } = window.location;
	const slug = pathname.replace(/^\/+/gi, '');

	return <PageLoader endpoint="v1/resume" payload={{ slug }} Component={Resume} />;
}
