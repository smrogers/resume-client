if (ENVIRONMENT === 'development') {
	require('preact/debug');
}

// -----------------------------------------------------------------------------

import(
	/* webpackChunkName: "co-ap" */
	'component/App'
).then((preloader) => preloader.default());
