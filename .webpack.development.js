require('dotenv').config();

// -----------------------------------------------------------------------------

const path = require('path');
const webpack = require('webpack');

const HtmlWebPackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

// -----------------------------------------------------------------------------

module.exports = {
	devServer: {
		clientLogLevel: 'error',
		host: '0.0.0.0',
		hot: true,
		inline: true,
		noInfo: true,
		port: process.env.PORT || 8888,
		proxy: {
			'/api': {
				target: process.env.API_HOST,
				pathRewrite: { '^/api': '' },
			},
		},
		historyApiFallback: true,
	},

	devtool: 'eval-cheap-module-source-map',

	entry: './src/index.tsx',

	mode: 'development',

	module: {
		rules: [
			{
				test: /.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},

			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				loaders: [
					'babel-loader',
					{
						loader: 'ts-loader',
					},
				],
			},

			{
				test: /\.scss$/i,
				use: [
					{
						loader: 'style-loader',
						options: {},
					},
					{
						loader: 'css-loader',
						options: {
							modules: {
								localIdentName: '[path][local]',
							},
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						loader: 'sass-loader',
						options: {
							// prependData: '@import "App/include.scss";',
							sassOptions: {
								includePaths: [__dirname, 'src'],
							},
							sourceMap: true,
						},
					},
				],
			},

			{
				test: /.html$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'html-loader',
					},
				],
			},
		],
	},

	optimization: {
		minimize: false,
	},

	output: {
		chunkFilename: '[name].[chunkhash].js',
		globalObject: 'window',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
	},

	plugins: [
		new webpack.DefinePlugin({
			ENVIRONMENT: JSON.stringify(process.env.ENVIRONMENT || 'development'),
			EDITOR_URL: JSON.stringify(process.env.EDITOR_URL),
		}),
		new HtmlWebPackPlugin({
			template: './src/index.html',
			filename: './index.html',
			scriptLoading: 'defer',
		}),
		new ProgressBarPlugin({
			width: 80,
			format: '[:bar] :percent (:elapseds)',
		}),
	],

	resolve: {
		alias: {
			'react': 'preact/compat',
			'react-dom/test-utils': 'preact/test-utils',
			'react-dom': 'preact/compat',
		},
		extensions: ['.js', '.ts', '.tsx'],
		modules: [path.resolve('./src'), path.resolve('./node_modules')],
	},
};
