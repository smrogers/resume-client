# Steven Rogers' Resume Client

The resume client uses webpack to compile and run a Typescript-enabled preact/storeon single page app.

The build files is hosted in an S3 bucket attached to an API gateway endpoint that proxies local /api/\* requests to the API server.

## Running locally

Create an .env file:

```bash
ENVIRONMENT=development
EDITOR_URL=http://localhost:8889
API_HOST=http://localhost:8889
PORT=8888
```

Then `npm install` and `npm start`.

## Deploying to AWS

- Have local aws credentials profile called resume with generous S3 permissions
- Create an .env.production file:

```bash
ENVIRONMENT=production
EDITOR_URL=https://realeditorhost.com
```

- `npm run deploy-production`

## Other repos

**Resume Editor Client**: https://bitbucket.org/smrogers/resume-editor-client

**API Server**: https://bitbucket.org/smrogers/resume-server
