type Environment = 'development' | 'production';

type GenericObject = { [key: string]: any };

// -----------------------------------------------------------------------------

declare var ENVIRONMENT: Environment;
declare var EDITOR_URL: string;

declare module '*.scss' {
	export const content: { [className: string]: string };
	export default content;
}
